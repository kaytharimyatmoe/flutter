class Category {
  String category;
  String description;
  String id;
  String image;

  Category({this.category, this.description, this.id, this.image});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      category: json['category'],
      description: json['description'],
      id: json['id'],
      image: json['image'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['description'] = this.description;
    data['id'] = this.id;
    data['image'] = this.image;
    return data;
  }

  Map<String, dynamic> toDatabaseJson() => {
        'category': this.category,
        'description': this.description,
        'id': this.id,
        'image': this.image
      };
}
