import 'dart:async';
import 'dart:convert' show json;
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import 'data_connection_checker.dart';

class RemoteDataSource {
  final _host = '139.59.101.3';
  final _port = ':3050';
  final _apiRoot = 'api';
  final _apiService = 'services';

  Future<Map> apiRequest(
      {@required String path, Map<String, String> parameters}) async {
    log('data: $_apiRoot/$path');
    try {
      /*final result = await InternetAddress.lookup(_host);
        log('start check');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {*/
      if (await DataConnectionChecker().hasConnection) {
        log('connected => ' + _host + _port);
        final uri = Uri.http(_host + _port, '$_apiRoot/$path');
        final results = await http.get(uri, headers: _headers);
        if (results.statusCode == 200) {
          log(json.decode(results.body));
          return json.decode(results.body);
        } else {
          // If that response was not OK, throw an error.
          throw Exception('Failed to load post');
        }
      }
    } on Exception catch (e) {
      log('not connected');
      return null;
    }

    return null;
  }

  Future<Map> serviceRequest(
      {@required String path, Map<String, String> parameters}) async {
    final uri = Uri.http(_host, '$_apiService/$path', parameters);
    final results = await http.get(uri, headers: _headers);
    if (results.statusCode == 200) {
      return json.decode(results.body);
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Map<String, String> get _headers => {'Accept': 'application/json'};
}
