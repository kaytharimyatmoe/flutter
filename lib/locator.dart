import 'package:get_it/get_it.dart';
import 'package:provider_to_mobx/category_screen/category_dao.dart';
import 'package:provider_to_mobx/category_screen/category_repository.dart';
import 'package:provider_to_mobx/category_screen/category_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => CategoryService());
  locator.registerLazySingleton(() => CategoryDao());
  locator.registerLazySingleton(() => CategoryRepository());
}
