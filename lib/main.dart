import 'package:flutter/material.dart';
import 'package:provider_to_mobx/locator.dart';
import 'package:provider_to_mobx/router.dart';
import 'package:provider_to_mobx/ui/resource/app_colors.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: appThemeColor,
      ),
      initialRoute: '/',
      onGenerateRoute: Router.generateRoute,
    );
  }
}
