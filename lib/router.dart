import 'package:flutter/material.dart';
import 'package:provider_to_mobx/category_screen/category_view.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'category':
        return MaterialPageRoute(builder: (context) => CategoryView());
      default:
        return MaterialPageRoute(builder: (context) => CategoryView());
    }
  }
}
