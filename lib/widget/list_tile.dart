import 'package:flutter/material.dart';
import 'package:provider_to_mobx/data/models/category_model.dart';

class MyListTile extends StatelessWidget {
  final Object obj;

  const MyListTile({Key key, @required this.obj}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return obj is Category ? categoryListTile(obj as Category) : null;
  }

  Widget categoryListTile(Category category) => ListTile(
        title: Text(category.category),
        leading: CircleAvatar(
          backgroundImage:
              NetworkImage("http://139.59.101.3:3050/" + category.image),
        ),
      );
}
