import 'package:mobx/mobx.dart';
import 'package:provider_to_mobx/category_screen/category_repository.dart';
import 'package:provider_to_mobx/data/models/category_model.dart';
import 'package:provider_to_mobx/enum/viewstate.dart';
import 'package:provider_to_mobx/locator.dart';

part 'category_viewmodel.g.dart';

class CategoryViewModel = CategoryViewModelBase with _$CategoryViewModel;

abstract class CategoryViewModelBase with Store {
  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  @observable
  ViewState viewState;

  @observable
  List<Category> categoryList = List.of([]);

  @computed
  int get categoryCount => categoryList.length;

  @action
  Future<void> getCategories() async {
    viewState = ViewState.Busy;
    await _categoryRepository.fetchCategory().then((value) {
      categoryList = List.of(value);
      viewState = ViewState.Done;
    }).catchError(() {
      viewState = ViewState.Failed;
    });
  }
}
