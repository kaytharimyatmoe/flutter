import 'package:provider_to_mobx/data/api/api_datasources.dart';
import 'package:provider_to_mobx/data/models/category_model.dart';

class CategoryService extends RemoteDataSource {
  Future<List<Category>> fetchCategory() async {
    final results = await apiRequest(path: 'category');

    final categorys = results['category'];

    return categorys
        .map<Category>((json) => Category.fromJson(json))
        .toList(growable: false);
  }
}
