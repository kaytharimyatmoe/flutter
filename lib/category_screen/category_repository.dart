import 'package:provider_to_mobx/category_screen/category_service.dart';
import 'package:provider_to_mobx/data/models/category_model.dart';
import 'package:provider_to_mobx/locator.dart';

import 'category_dao.dart';

class CategoryRepository {
  final CategoryDao _categoryDao = locator<CategoryDao>();
  final CategoryService _categoryService = locator<CategoryService>();

  Future<List<Category>> fetchCategory() async {
    var results = await _categoryService.apiRequest(path: 'category');
    if (results != null) {
      final categorys = results['category'];
      _categoryDao.batchinsertCategory(categorys
          .map<Category>((json) => Category.fromJson(json))
          .toList(growable: false));
    }

    return _categoryDao.getCategory();
  }
}
