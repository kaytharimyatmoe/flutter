import 'package:provider_to_mobx/data/datasource/local_datasources.dart';
import 'package:provider_to_mobx/data/models/category_model.dart';
import 'package:sqflite/sqlite_api.dart';

class CategoryDao {
  final dbProvider = LocalDataSource.dbProvider;

  Future<List> batchinsertCategory(List<Category> categories) async {
    final db = await dbProvider.database;
    Batch batch = db.batch();
    batch.delete(categoryTable);
    categories.forEach((val) {
      batch.insert(categoryTable, val.toDatabaseJson());
    });
    var result = batch.commit();
    return result;
  }

  Future<List<Category>> getCategory(
      {List<String> columns, String query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    if (query != null) {
      if (query.isNotEmpty)
        result = await db.query(categoryTable,
            columns: columns,
            where: 'category LIKE ?',
            whereArgs: ["%$query%"]);
    } else {
      result = await db.query(categoryTable, columns: columns);
    }

    List<Category> category = result.isNotEmpty
        ? result.map<Category>((item) => Category.fromJson(item)).toList()
        : [];
    return category;
  }
}
