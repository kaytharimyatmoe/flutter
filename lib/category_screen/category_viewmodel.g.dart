// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_viewmodel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CategoryViewModel on CategoryViewModelBase, Store {
  Computed<int> _$categoryCountComputed;

  @override
  int get categoryCount =>
      (_$categoryCountComputed ??= Computed<int>(() => super.categoryCount))
          .value;

  final _$viewStateAtom = Atom(name: 'CategoryViewModelBase.viewState');

  @override
  ViewState get viewState {
    _$viewStateAtom.context.enforceReadPolicy(_$viewStateAtom);
    _$viewStateAtom.reportObserved();
    return super.viewState;
  }

  @override
  set viewState(ViewState value) {
    _$viewStateAtom.context.conditionallyRunInAction(() {
      super.viewState = value;
      _$viewStateAtom.reportChanged();
    }, _$viewStateAtom, name: '${_$viewStateAtom.name}_set');
  }

  final _$categoryListAtom = Atom(name: 'CategoryViewModelBase.categoryList');

  @override
  List<Category> get categoryList {
    _$categoryListAtom.context.enforceReadPolicy(_$categoryListAtom);
    _$categoryListAtom.reportObserved();
    return super.categoryList;
  }

  @override
  set categoryList(List<Category> value) {
    _$categoryListAtom.context.conditionallyRunInAction(() {
      super.categoryList = value;
      _$categoryListAtom.reportChanged();
    }, _$categoryListAtom, name: '${_$categoryListAtom.name}_set');
  }

  final _$getCategoriesAsyncAction = AsyncAction('getCategories');

  @override
  Future<void> getCategories() {
    return _$getCategoriesAsyncAction.run(() => super.getCategories());
  }
}
