import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider_to_mobx/category_screen/category_viewmodel.dart';
import 'package:provider_to_mobx/data/models/category_model.dart';
import 'package:provider_to_mobx/enum/viewstate.dart';
import 'package:provider_to_mobx/widget/list_tile.dart';

class CategoryView extends StatefulWidget {
  @override
  _CategoryViewState createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  final CategoryViewModel _categoryViewModel = CategoryViewModel();

  @override
  void initState() {
    _categoryViewModel.getCategories();
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Observer(
            builder: (_) => _categoryViewModel.categoryCount > 0
                ? Text('I Got ${_categoryViewModel.categoryCount} Categories')
                : Text('Category'),
          ),
        ),
        body: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () => _categoryViewModel.getCategories(),
          child: Observer(
              builder: (context) =>
                  renderCategoryListView(_categoryViewModel.viewState)),
        ));
  }

  Widget renderCategoryListView(ViewState viewState) {
    switch (viewState) {
      case ViewState.Busy:
        {
          _refreshIndicatorKey.currentState.show();
          return Container();
        }
      case ViewState.Done:
        {
          List<Category> list = _categoryViewModel.categoryList;
          return ListView.separated(
              itemBuilder: (context, index) => MyListTile(obj: list[index]),
              separatorBuilder: (_, __) => Divider(),
              itemCount: list.length);
        }
      case ViewState.Failed:
        return Center(
          child: Text('Failed'),
        );
      default:
        return Center(
          child: Text('No results yet'),
        );
        break;
    }
  }
}
